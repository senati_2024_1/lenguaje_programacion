# Repositorio - Sesiones de aprendizaje 202410
## Curso: Lenguaje de programación
## Instructor: Ing. Franklin Condori

## Libros de consulta

* [Libro Sumérgete en los PATRONES DE DISEÑO](https://drive.google.com/file/d/1EWPS2-ZWKsLF6yrMkHfaoBrdTZmWxtn_/view?usp=sharing)


## Manuales

* [Introducción](/contenido/introduccion.md)

## Presentaciones - Sesiones aprendizaje


## Materiales audiovisuales

## Trabajos para la calificación de seminario

* [Tarea 04: Resolver los ejercicios propuestos](./tareas/linq/tarea_linq.md)

### Trabajos para la casa

* [Tarea 01](https://drive.google.com/file/d/1fUer9IlNhvmheXv4BBu7fuew9cXteAgF/view?usp=drive_link)
* [Tarea 02: Ejercicios propuestos](tareas/02/tarea02.md)

### Prácticas de clase

* [Ejercicios de programación](https://drive.google.com/file/d/1H_YycCe5BmRWbhqd2MN4XqZkZMWECu5X/view?usp=sharing)
* [Ejemplo Menú C#](contenido/ejemplo.md)
* [1. Creando nuestro proyecto](/practicas/crear_proyecto.md)
* [2. Resolver los ejercicios resuelto y propuesto del manua de TransactSQL](./practicas/ejercicios-transactsql.md)
* [3. Creando procedimientos almacenados](/practicas/procedimientos_almacenados.md)
* [4. Clonado nuestro proyecto -- git clone https://gitlab.com/senati_2024_1/SistemaEducativo.git](https://gitlab.com/senati_2024_1/SistemaEducativo.git)