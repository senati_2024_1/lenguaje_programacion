# Crear Proyecto "SistemaEducativo"

## Sistema de Ventas

En el Presente proyecto, crearemos un sistema de ventas desktop en C# empleando la arquitectura de N-Capas.

## Arquitectura del sistema

![arquitectura](/practicas/crear_proyecto/arquitectura/n-capas.png)

## Base de datos del sistema

* [Modelado de la base de datos](../practicas/crear_proyecto/base_datos/base_datos.md)

* [base_datos](/practicas/crear_proyecto/base_datos/db_sistema_educativo.bak)

## Agregando Conexion a la base de datos

Crearemos una clase Conexion.cs dentro del proyecto Sistema.Datos y ahrehamos el siguiente codigo

```c#:
    public class Conexion
        {
            private string Base;
            private string Servidor;
            private string Usuario;
            private string Clave;
            private bool Seguridad;
            private static Conexion Con = null;

            private Conexion()
            {
                this.Base = "dbsistema";
                this.Servidor = "DESKTOP-...";
                this.Usuario = "sa";
                this.Clave = "frank";
                this.Seguridad = true;
            }
            public SqlConnection CrearConexion()
            {
                SqlConnection Cadena = new SqlConnection();
                try
                {
                    Cadena.ConnectionString = "Server=" + this.Servidor + "; Database=" + this.Base + ";";
                    if (this.Seguridad)
                    {
                        Cadena.ConnectionString = Cadena.ConnectionString + "Integrated Security = SSPI";
                    }
                    else
                    {
                        Cadena.ConnectionString = Cadena.ConnectionString + "User Id=" + this.Usuario + ";Password=" + this.Clave;
                    }
                }
                catch (Exception ex)
                {
                    Cadena = null;
                    throw ex;
                }
                return Cadena;
            }

            public static Conexion getInstancia()
            {
                if (Con == null)
                {
                    Con = new Conexion();
                }
                return Con;
            }
        }
```

## Agregamos las entidades 

[Entidades](https://drive.google.com/file/d/1X9VJRK4ldfACO50aD8D6ikqJB5iwmwAe/view?usp=sharing)


## Agregamos las clases de la capa Datos

[Datos](https://drive.google.com/file/d/1fY6yTwVnWSLZPVejukSOgGvhL5Q5Qp-J/view?usp=sharing)


## Agregamos las clases de la capa Negocio

[Negocio](https://drive.google.com/file/d/1yWh__v21FAhuvh8VDX5E4yxuzVBxi_1j/view?usp=sharing)
