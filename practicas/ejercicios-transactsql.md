# Resolver los ejercicios resueltos y propuesto del manual de Transact-SQL

## Material de consulta

* [Manual Transact-SQL](https://drive.google.com/file/d/1WkejWDw240TcgBTB56tIU3pMsfqRTWE5/view?usp=sharing)

### Backup de las bases de datos

* [EDUCA](https://drive.google.com/drive/folders/1AZfCYXzrnlLecFvmIbljKmvNA0n-1G6S?usp=sharing)
* [EDUTEC](https://drive.google.com/drive/folders/1IweK-Oq1TQqUDvJTmGlrsN0gmpDzJs6w?usp=sharing)
* [EUREKABANK](https://drive.google.com/drive/folders/1L-eAUBmGOdRvbTUELBtcAEiedcWg-Pe8?usp=sharing)
* [Northwind](https://drive.google.com/drive/folders/1wHWyK4rE5G5em1gMkQvptX3jAZ5QcWzH?usp=sharing)
* [RRHH](https://drive.google.com/drive/folders/1kAlLIzYCFrnoTyrNayGSN1HDz7ijuAhJ?usp=sharing)