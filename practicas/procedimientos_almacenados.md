# Creando procedimientos almacenados

## Procedimientos almacenados para la tabla "Persona"

```sql:
use db_sistema_educativo
go

-- Procedimiento almacenado para listar todas las personas
CREATE PROCEDURE ListarPersonas
AS
BEGIN
    SELECT * FROM Persona
END
GO

-- Procedimiento almacenado para buscar una persona por DNI
CREATE PROCEDURE BuscarPersonaPorDNI
    @DNI VARCHAR(8)
AS
BEGIN
    SELECT * FROM Persona WHERE dni = @DNI
END
GO

-- Procedimiento almacenado para insertar una nueva persona
CREATE PROCEDURE InsertarPersona
    @DNI VARCHAR(8),
    @Estado BIT,
    @RUC VARCHAR(11),
    @ApellidosPaterno VARCHAR(50),
    @ApellidoMaterno VARCHAR(50),
    @Nombres VARCHAR(50),
    @Edad INT,
    @Sexo CHAR(1),
    @Foto VARCHAR(255),
    @Email VARCHAR(100)
AS
BEGIN
    INSERT INTO Persona (dni, estado, ruc, apellidos_paterno, apellido_materno, nombres, edad, sexo, foto, email)
    VALUES (@DNI, @Estado, @RUC, @ApellidosPaterno, @ApellidoMaterno, @Nombres, @Edad, @Sexo, @Foto, @Email)
END
GO

-- Procedimiento almacenado para actualizar los datos de una persona
CREATE PROCEDURE ActualizarPersona
    @DNI VARCHAR(8),
    @Estado BIT,
    @RUC VARCHAR(11),
    @ApellidosPaterno VARCHAR(50),
    @ApellidoMaterno VARCHAR(50),
    @Nombres VARCHAR(50),
    @Edad INT,
    @Sexo CHAR(1),
    @Foto VARCHAR(255),
    @Email VARCHAR(100)
AS
BEGIN
    UPDATE Persona
    SET estado = @Estado, ruc = @RUC, apellidos_paterno = @ApellidosPaterno, 
        apellido_materno = @ApellidoMaterno, nombres = @Nombres, edad = @Edad, 
        sexo = @Sexo, foto = @Foto, email = @Email
    WHERE dni = @DNI
END
GO

-- Procedimiento almacenado para eliminar una persona
CREATE PROCEDURE EliminarPersona
    @DNI VARCHAR(8)
AS
BEGIN
    DELETE FROM Persona WHERE dni = @DNI
END
GO


-- Ejecutar el procedimiento almacenado ListarPersonas
EXEC ListarPersonas;

-- Ejecutar el procedimiento almacenado BuscarPersonaPorDNI con un parámetro
DECLARE @DNI varchar(8);
SET @DNI = '12345678';

EXEC BuscarPersonaPorDNI @DNI;
```