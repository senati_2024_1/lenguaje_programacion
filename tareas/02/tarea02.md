# Ejercicios de programación

Resuelva los ejercicios empleando el programa PSeint

Fuente: [libro de consulta](https://editorial.uaa.mx/docs/algoritmos.pdf)

## Ejercicios básicos

1. Una empresa que contrata personal requiere determinar la edad de las personas que solicitan trabajo, pero cuando se les realiza la entrevista sólo se les pregunta el año en que nacieron. Realice el diagrama de flujo y pseudocódigo que representen el algoritmo para solucionar este problema. 

2. Pinturas “La brocha gorda” requiere determinar cuánto cobrar por trabajos de pintura. Considere que se cobra por m2 y realice un diagrama de flujo y pseudocódigo que representen el algoritmo que le permita ir generando presupuestos para cada cliente.

3. Se requiere determinar el tiempo que tarda una persona en llegar de una ciudad a otra en bicicleta, considerando que lleva una velocidad constante. Realice un diagrama de flujo y pseudocódigo que representen el algoritmo para tal fin.

4. Se requiere determinar el costo que tendrá realizar una llamada telefónica con base en el tiempo que dura la llamada y en el costo por minuto. Realice un diagrama de flujo y pseudocódigo que representen el algoritmo para tal fin. 

5. Se desea calcular la potencia eléctrica de circuito de la figura. Realice un diagrama de flujo y el pseudocódigo que representen el algoritmo para resolver el problema. Considere que: P = V*I y V = R*I

![0](/tareas/02/0.png)

## Ejercicios empleando condicionales

6. Realice un algoritmo que, con base en una calificación proporcionada (0-10), indique con letra la calificación que le corresponde: 10 es “A”, 9 es “B”, 8 es “C”, 7 y 6 son “D”, y de 5 a 0 son “F”. Represente el diagrama de flujo y su pseudocódigo.

7. El 14 de febrero una persona desea comprarle un regalo al ser querido que más aprecia en ese momento, su dilema radica en qué regalo puede hacerle, las alternativas que tiene son las siguientes:

![1](/tareas/02/1.png)

8. Se les dará un bono por antigüedad a los empleados de una tienda. Si tienen un año, se les dará $100; si tienen 2 años, $200, y así sucesivamente hasta los 5 años. Para los que tengan más de 5, el bono será de $1000. Realice un algoritmo y represéntelo mediante el diagrama de flujo, el pseudocódigo y diagrama N/S que permita determinar el bono que recibirá un trabajador.

9. El secretario de educación ha decidido otorgar un bono por desempeño a todos los profesores con base en la puntuación siguiente:

![2](/tareas/02/2.png)

Realice un algoritmo que permita determine el monto de bono que percibirá un profesor (debe capturar el valor del salario mínimo y los puntos del profesor). Represente el algoritmo mediante el diagrama de flujo, el pseudocódigo y el diagrama N/S.

10. Realice un algoritmo para resolver el siguiente problema: una fábrica de pantalones desea calcular cuál es el precio final de venta y cuánto ganará por los N pantalones que produzca con el corte de alguno de sus modelos, para esto se cuenta con la siguiente información:

    a) Tiene dos modelos A y B, tallas 30, 32 y 36 para ambos modelos.

    b) Para el modelo A se utiliza 1.50 m de tela, y para el B 1.80 m.

    c) Al modelo A se le carga 80 % del costo de la tela, por mano de obra. Al modelo B se le carga 95 % del costo de la tela, por el mismo concepto.

## Ejercicios empleando iteraciones

11. Realice un algoritmo y represéntelo mediante un diagrama de flujo para obtener el factorial de un numero.

12. Realice un algoritmo y represéntelo mediante un diagrama de flujo para obtener una función exponencial, la cual está dada por:

![3](/tareas/02/3.png)

13. Realice un algoritmo que determine el sueldo semanal de N trabajadores considerando que se les descuenta 5% de su sueldo si ganan entre 0 y 150 pesos. Se les descuenta 7% si ganan más de 150 pero menos de 300, y 9% si ganan más de 300 pero menos de 450. Los datos son horas trabajadas, sueldo por hora y nombre de cada trabajador. Represéntelo mediante diagrama de flujo, pseudocódigo y diagrama N/S.

14. Realice un algoritmo para obtener el seno de un ángulo y represéntelo mediante diagrama de flujo, pseudocódigo y diagrama N/S.

![4](/tareas/02/4.png)

15. El banco “Bandido de peluche” desea calcular para cada uno de sus N clientes su saldo actual, su pago mínimo y su pago para no generar intereses. Además, quiere calcular el monto de lo que ganó por concepto interés con los clientes morosos. Los datos que se conocen de cada cliente son: saldo anterior, monto de las compras que realizó y pago que depositó en el corte anterior. Para calcular el pago mínimo se considera 15% del saldo actual, y el pago para no generar intereses corresponde a 85% del saldo actual, considerando que el saldo actual debe incluir 12% de los intereses causados por no realizar el pago mínimo y $200 de multa por el mismo motivo. Realice el algoritmo correspondiente y represéntelo mediante diagrama de flujo y pseudocódigo.


## Ejercicios empleando vectores

* [Material de consulta para vectores en PSeint](https://vochoa84.files.wordpress.com/2016/03/vectores-pseint.pdf)

16. Realice un algoritmo para obtener el vector resultante de dos vectores, las entradas de los vectores $\vec{a} = (x_{1},y_{1})$ y $\vec{b} = (x_{2},y_{2})$ deben ser almacenadas en un arreglo de 1x2 para su calculo y se debe considerar el angulo formado por ambos vectores.

17. Realice un algoritmo que lea un vector y a partir de él forme un segundo vector, de tal forma que el primer elemento pase a ser el segundo, el segundo pase a ser el tercero, el último pase a ser el primero, y así sucesivamente. Represéntelo mediante un diagrama de flujo.


18. Una compañía de transporte cuenta con cinco choferes, de los cuales se conoce: nombre, horas trabajadas cada día de la semana (seis días) y sueldo por hora. Realice un algoritmo que:

    a) Calcule el total de horas trabajadas a la semana para cada trabajador.

    b) Calcule el sueldo semanal para cada uno de ellos.

    c) Calcule el total que pagará la empresa.

    d) Indique el nombre del trabajador que labora más horas el día lunes.

    e) Imprima un reporte con todos los datos anteriores.

19. Se tiene una matriz de 12 filas por 19 columnas y se desea un algoritmo para encontrar todos sus elementos negativos y para que les cambie ese valor negativo por un cero. Realice un algoritmo para tal fin y represéntelo mediante diagrama N/S y pseudocódigo.

20. Realice un algoritmo que calcule el valor que se obtiene al multiplicar entre sí los elementos de la diagonal principal de una matriz de 5 por 5 elementos, represéntelo mediante diagrama, diagrama N/S y pseudocódigo.
