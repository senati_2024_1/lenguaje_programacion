# Desarrollar los siguientes ejercicios

# Ejecutar los siguientes ejemplos y desarrollar los siguientes ejercicios

**1.	Escriba un programa en C# para mostrar cómo se ejecutan las tres partes de una operación de consulta.**

```sh:
Resultado esperado:
Los números que producen el resto 0 después de dividido por 2 son: 0 2 4 6 8
```
Solucion:

```c#:
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Crear una lista de números del 0 al 9
        var numeros = Enumerable.Range(0, 10);

        // Consulta LINQ para seleccionar números que producen un resto de 0 al dividir por 2
        var numerosPares = from num in numeros
                           where num % 2 == 0
                           select num;

        // Mostrar el resultado
        Console.WriteLine("Los números que producen el resto 0 después de dividido por 2 son:");
        foreach (var num in numerosPares)
        {
            Console.Write(num + " ");
        }
        Console.ReadLine(); // Para mantener la consola abierta hasta que se presione Enter
    }
}
```

**2.	Escriba un programa en C# para encontrar los números de más de una lista de números usando dos condiciones en la consulta LINQ.**

```sh:
Resultado esperado:
Los números dentro del rango de 1 a 11 son:
1 3 6 9 10
```

Solucion:

```c#:
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Definir dos listas de números
        var lista1 = new int[] { 1, 3, 5, 7, 9, 11 };
        var lista2 = new int[] { 2, 3, 4, 6, 8, 10 };

        // Consulta LINQ para encontrar los números comunes en ambas listas dentro del rango de 1 a 11
        var numerosComunes = from num1 in lista1
                             from num2 in lista2
                             where num1 >= 1 && num1 <= 11 && num2 >= 1 && num2 <= 11
                             where num1 == num2
                             select num1;

        // Eliminar duplicados y ordenar los resultados
        var numerosUnicos = numerosComunes.Distinct().OrderBy(n => n);

        // Mostrar el resultado
        Console.WriteLine("Los números dentro del rango de 1 a 11 son:");
        foreach (var num in numerosUnicos)
        {
            Console.Write(num + " ");
        }
        Console.ReadLine(); // Para mantener la consola abierta hasta que se presione Enter
    }
}
```

**3.	Escriba un programa en C# para encontrar el número de una matriz y el cuadrado de cada número.**

```sh:
Resultado esperado:
{Número = 9, SqrNo = 81}
{Número = 8, SqrNo = 64}
{Número = 6, SqrNo = 36}
{Número = 5, SqrNo = 25}
```

Solucion:

```c#:
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Definir una matriz de números
        int[] numeros = { 9, 8, 6, 5 };

        // Consulta LINQ para encontrar el número y el cuadrado de cada número en la matriz
        var numerosConCuadrado = from num in numeros
                                 select new { Numero = num, SqrNo = num * num };

        // Mostrar el resultado
        foreach (var item in numerosConCuadrado)
        {
            Console.WriteLine($"{{Número = {item.Numero}, SqrNo = {item.SqrNo}}}");
        }
        Console.ReadLine(); // Para mantener la consola abierta hasta que se presione Enter
    }
}
```

**4.	Escriba un programa en C# para mostrar el número y la frecuencia del número de la matriz.**

```sh:
Salida esperada:
el número y la frecuencia son:
el número 5 aparece 3 veces
el número 9 aparece 2 veces
el número 1 aparece 1 veces
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Definir una matriz de números
        int[] numeros = { 5, 9, 5, 1, 5, 9 };

        // Consulta LINQ para agrupar los números y contar su frecuencia
        var frecuenciaNumeros = numeros.GroupBy(num => num)
                                       .Select(group => new { Numero = group.Key, Frecuencia = group.Count() });

        // Mostrar el resultado
        foreach (var item in frecuenciaNumeros)
        {
            Console.WriteLine($"el número {item.Numero} aparece {item.Frecuencia} veces");
        }
        Console.ReadLine(); // Para mantener la consola abierta hasta que se presione Enter
    }
}
```

**5.	Escriba un programa en C# para mostrar los caracteres y la frecuencia de los caracteres de una cadena.**

```sh: 
Datos de prueba:
ingrese la cadena: manzana
Salida esperada :
la frecuencia de los caracteres es:
Carácter a: 1 veces Carácter p: 2 veces Carácter l: 1 veces Carácter e: 1 veces
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Solicitar al usuario que ingrese una cadena
        Console.Write("Ingrese la cadena: ");
        string cadena = Console.ReadLine();

        // Consulta LINQ para agrupar los caracteres y contar su frecuencia
        var frecuenciaCaracteres = cadena.GroupBy(c => c)
                                         .Select(group => new { Caracter = group.Key, Frecuencia = group.Count() });

        // Mostrar el resultado
        Console.WriteLine("La frecuencia de los caracteres es:");
        foreach (var item in frecuenciaCaracteres)
        {
            Console.WriteLine($"Carácter {item.Caracter}: {item.Frecuencia} veces");
        }
        Console.ReadLine(); // Para mantener la consola abierta hasta que se presione Enter
    }
}
```

**6.	Escriba un programa en C# para mostrar el nombre de los días de la semana.**

```sh:
Salida prevista :
domingo lunes martes miércoles jueves viernes sábado
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Lista predefinida de nombres de días de la semana
        List<string> diasSemana = new List<string>
        {
            "domingo", "lunes", "martes", "miércoles", "jueves", "viernes", "sábado"
        };

        // Mostrar los nombres de los días de la semana utilizando LINQ
        string diasSemanaString = string.Join(" ", diasSemana.Select(dia => dia));

        // Mostrar los nombres de los días de la semana en la consola
        Console.WriteLine(diasSemanaString);
    }
}
```

**7.	Escriba un programa en C# para mostrar números, multiplicación de números con la frecuencia y la frecuencia de un número de matriz de dar.**

```sh:
Datos de prueba:
Los números en la matriz son:
5, 1, 9, 2, 3, 7, 4, 5, 6, 8, 7, 6, 3, 4, 5, 2
Resultado esperado :
Número Frecuencia Número * Frecuencia 
5 3 15
1 1 1
9 1 9
2 2 4
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: los números en la matriz
        int[] numeros = { 5, 1, 9, 2, 3, 7, 4, 5, 6, 8, 7, 6, 3, 4, 5, 2 };

        // Consulta LINQ para agrupar los números y contar su frecuencia
        var resultado = numeros.GroupBy(num => num)
                               .Select(group => new
                               {
                                   Numero = group.Key,
                                   Frecuencia = group.Count(),
                                   Multiplicacion = group.Key * group.Count() // Multiplicar número por frecuencia
                               });

        // Mostrar el resultado
        Console.WriteLine("Número Frecuencia Número * Frecuencia");
        foreach (var item in resultado)
        {
            Console.WriteLine($"{item.Numero,-7}{item.Frecuencia,-11}{item.Multiplicacion}");
        }
    }
}
```

**8.	Escriba un programa en C# para encontrar la cadena que comienza y termina con un carácter específico.**

```sh:
de datos de prueba:
Las ciudades son: 'Roma', 'LONDRES', 'NAIROBI', 'CALIFORNIA', 'ZURICH', 'Nueva Delhi', 'AMSTERDAM', 'ABU DHABI', 'Paris'
A partir de entrada carácter para la cadena: A Entrada carácter final para la cadena: M Salida esperada :
la ciudad que comienza con A y termina con M es: AMSTERDAM
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Lista de ciudades
        List<string> ciudades = new List<string> { "Roma", "LONDRES", "NAIROBI", "CALIFORNIA", "ZURICH", "Nueva Delhi", "AMSTERDAM", "ABU DHABI", "Paris" };

        // Solicitar al usuario que ingrese los caracteres de inicio y fin
        Console.Write("Ingrese el carácter de inicio para la cadena: ");
        char inicio = char.Parse(Console.ReadLine());
        Console.Write("Ingrese el carácter final para la cadena: ");
        char fin = char.Parse(Console.ReadLine());

        // Utilizar LINQ para encontrar la ciudad que cumple con los criterios
        string ciudadEncontrada = ciudades.FirstOrDefault(ciudad =>
            ciudad.StartsWith(inicio.ToString(), StringComparison.OrdinalIgnoreCase) &&
            ciudad.EndsWith(fin.ToString(), StringComparison.OrdinalIgnoreCase)
        );

        // Mostrar el resultado
        if (ciudadEncontrada != null)
        {
            Console.WriteLine($"La ciudad que comienza con {inicio} y termina con {fin} es: {ciudadEncontrada}");
        }
        else
        {
            Console.WriteLine("No se encontró ninguna ciudad que cumpla con los criterios.");
        }
    }
}
```

**9.	Escriba un programa en C# para crear una lista de números y mostrar los números mayores a 80 como salida.**

```sh:
Datos de prueba:
Los miembros de la lista son:
55 200 740 76 230 482 95
Resultado esperado :
Los números mayores que 80 son:
200
740
230
482
95
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: los miembros de la lista
        List<int> numeros = new List<int> { 55, 200, 740, 76, 230, 482, 95 };

        // Utilizar LINQ para seleccionar los números mayores que 80
        var numerosMayores80 = numeros.Where(num => num > 80);

        // Mostrar los números mayores que 80
        Console.WriteLine("Los números mayores que 80 son:");
        foreach (var numero in numerosMayores80)
        {
            Console.WriteLine(numero);
        }
    }
}
```

**10.	Escriba un programa en C# para aceptar los miembros de una lista a través del teclado y mostrar los miembros más que un valor específico.**

```sh:
Datos de prueba:
ingrese el número de miembros en la lista: 5 miembro 0: 10
miembro 1: 48
miembro 2: 52
miembro 3: 94
miembro 4: 63
ingrese el valor arriba para mostrar los miembros de la lista : 59
 
Resultado esperado :
los números mayores que 59 son:
94
63
```

Solucion:

```c#:
using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        // Solicitar al usuario el número de miembros en la lista
        Console.Write("Ingrese el número de miembros en la lista: ");
        int numMiembros = int.Parse(Console.ReadLine());

        // Crear una lista para almacenar los miembros
        List<int> miembros = new List<int>();

        // Solicitar al usuario que ingrese los miembros de la lista
        for (int i = 0; i < numMiembros; i++)
        {
            Console.Write($"Miembro {i}: ");
            int miembro = int.Parse(Console.ReadLine());
            miembros.Add(miembro);
        }

        // Solicitar al usuario el valor para comparar
        Console.Write("Ingrese el valor para mostrar los miembros de la lista: ");
        int valorComparar = int.Parse(Console.ReadLine());

        // Utilizar LINQ para seleccionar los miembros mayores que el valor especificado
        var miembrosMayores = miembros.FindAll(miembro => miembro > valorComparar);

        // Mostrar los miembros mayores que el valor especificado
        Console.WriteLine("Los números mayores que " + valorComparar + " son:");
        foreach (var miembro in miembrosMayores)
        {
            Console.WriteLine(miembro);
        }
    }
}
```

**11.	Escriba un programa en C# para mostrar los registros número n superiores.**

```sh:
Datos de prueba:
Los miembros de la lista son:
5
7
13
24
6
9
8
7
¿Cuántos registros desea mostrar? : 3
Resultado esperado:
Los 3 primeros registros de la lista son:
24
13
9
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: los miembros de la lista
        List<int> numeros = new List<int> { 5, 7, 13, 24, 6, 9, 8, 7 };

        // Solicitar al usuario el número de registros a mostrar
        Console.Write("¿Cuántos registros desea mostrar? : ");
        int n = int.Parse(Console.ReadLine());

        // Utilizar LINQ para ordenar la lista en orden descendente y tomar los primeros 'n' registros
        var registrosTop = numeros.OrderByDescending(num => num).Take(n);

        // Mostrar los registros número 'n' superiores
        Console.WriteLine($"Los {n} primeros registros de la lista son:");
        foreach (var registro in registrosTop)
        {
            Console.WriteLine(registro);
        }
    }
}
```

**12.	Escriba un programa en C# para encontrar las palabras en mayúsculas en una cadena.**

```sh:
Datos de prueba:
ingrese la cadena: esto ES una CADENA
Salida esperada : las
palabras de MAYÚSCULAS son: ES
CADENA
```

Solucion:

```c#:
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Solicitar al usuario que ingrese una cadena
        Console.Write("Ingrese la cadena: ");
        string cadena = Console.ReadLine();

        // Dividir la cadena en palabras
        string[] palabras = cadena.Split(' ');

        // Utilizar LINQ para encontrar las palabras en mayúsculas
        var palabrasMayusculas = palabras.Where(palabra => palabra == palabra.ToUpper());

        // Mostrar las palabras en mayúsculas
        Console.WriteLine("Las palabras en MAYÚSCULAS son:");
        foreach (var palabra in palabrasMayusculas)
        {
            Console.WriteLine(palabra);
        }
    }
}
```

**13.	Escriba un programa en C# para convertir una matriz de cadenas en una cadena**

```sh:
Datos de prueba:
Ingrese el número de cadenas que se almacenarán en la matriz: 3 Introduzca 3 cadenas para la matriz:
Elemento [0]: gato
Elemento [1]: perro
Elemento [2]: rata
Resultado esperado:
Aquí está la cadena debajo creado con elementos de la matriz anterior: gato, perro, rata
```

Solucion:

```c#:
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Solicitar al usuario el número de cadenas y crear la matriz
        Console.Write("Ingrese el número de cadenas que se almacenarán en la matriz: ");
        int numCadenas = int.Parse(Console.ReadLine());
        string[] matriz = new string[numCadenas];

        // Solicitar al usuario que ingrese las cadenas para la matriz
        for (int i = 0; i < numCadenas; i++)
        {
            Console.Write($"Elemento [{i}]: ");
            matriz[i] = Console.ReadLine();
        }

        // Utilizar LINQ para concatenar las cadenas en una sola cadena
        string cadena = string.Join(", ", matriz);

        // Mostrar la cadena resultante
        Console.WriteLine("Aquí está la cadena creada con elementos de la matriz anterior: " + cadena);
    }
}
```

**14.	Escriba un programa en C# para encontrar el punto número máximo de calificación alcanzado por los estudiantes de la lista de estudiantes.**

```sh:
Datos de prueba:
qué puntaje máximo (1º, 2º, 3º, ...) que desea encontrar: 3
Resultado esperado :
Id: 7, Nombre: David, alcanzado Puntaje: 750 Id: 10, Nombre: Jenny, logró Grade Point: 750
```

Solucion:

```c#:
using System;
using System.Collections.Generic;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de estudiantes
        List<Estudiante> estudiantes = new List<Estudiante>
        {
            new Estudiante { Id = 1, Nombre = "Juan", Puntaje = 600 },
            new Estudiante { Id = 2, Nombre = "María", Puntaje = 700 },
            new Estudiante { Id = 3, Nombre = "Pedro", Puntaje = 750 },
            new Estudiante { Id = 4, Nombre = "Ana", Puntaje = 700 },
            new Estudiante { Id = 5, Nombre = "Carlos", Puntaje = 800 },
            new Estudiante { Id = 6, Nombre = "Laura", Puntaje = 720 },
            new Estudiante { Id = 7, Nombre = "David", Puntaje = 750 },
            new Estudiante { Id = 8, Nombre = "Sofía", Puntaje = 720 },
            new Estudiante { Id = 9, Nombre = "Jorge", Puntaje = 670 },
            new Estudiante { Id = 10, Nombre = "Jenny", Puntaje = 750 }
        };

        // Solicitar al usuario el puntaje máximo deseado
        Console.Write("Qué puntaje máximo desea encontrar? : ");
        int puntajeMaximo = int.Parse(Console.ReadLine());

        // Utilizar LINQ para encontrar los estudiantes con el puntaje máximo deseado
        var estudiantesMaximoPuntaje = estudiantes.FindAll(estudiante => estudiante.Puntaje == puntajeMaximo);

        // Mostrar los estudiantes con el puntaje máximo deseado
        Console.WriteLine($"Los estudiantes con un puntaje máximo de {puntajeMaximo} son:");
        foreach (var estudiante in estudiantesMaximoPuntaje)
        {
            Console.WriteLine($"Id: {estudiante.Id}, Nombre: {estudiante.Nombre}, Puntaje alcanzado: {estudiante.Puntaje}");
        }
    }
}

class Estudiante
{
    public int Id { get; set; }
    public string Nombre { get; set; }
    public int Puntaje { get; set; }
}
```

**15.	Escriba un programa en el programa C# para contar extensiones de archivo y agrúpelo usando LINQ.**

```sh:
Datos de prueba:
Los archivos son: aaa.frx, bbb.TXT, xyz.dbf, abc.pdf aaaa.PDF, xyz.frt, abc.xml, ccc.txt, zzz.txt Resultado esperado :
Aquí está el grupo de extensión de los archivos: 1 archivo (s) con .frx Extension
3 archivo (s) con .txt Extension 1 archivo (s) con .dbf Extension 2 archivo (s) con .pdf Extension 1 archivo (s) con .frt Extension 1 Archivo (s) con .xml Extension
``` 

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de archivos
        List<string> archivos = new List<string> { "aaa.frx", "bbb.TXT", "xyz.dbf", "abc.pdf", "aaaa.PDF", "xyz.frt", "abc.xml", "ccc.txt", "zzz.txt" };

        // Utilizar LINQ para contar las extensiones de archivo y agruparlas
        var gruposExtensiones = archivos
            .Select(archivo => archivo.Substring(archivo.LastIndexOf('.') + 1).ToLower()) // Extraer y convertir las extensiones a minúsculas
            .GroupBy(extension => extension) // Agrupar por extensión
            .OrderBy(group => group.Key); // Ordenar por extensión

        // Mostrar el resultado
        Console.WriteLine("Aquí está el grupo de extensiones de los archivos:");
        foreach (var grupo in gruposExtensiones)
        {
            string archivoPlural = grupo.Count() == 1 ? "archivo" : "archivos";
            Console.WriteLine($"{grupo.Count()} {archivoPlural} con .{grupo.Key.ToUpper()} Extension");
        }
    }
}
``` 

**16.	Escriba un programa en C# para calcular el tamaño del archivo usando LINQ.**

```sh:
Resultado esperado:
el tamaño promedio de archivo es de 3.4 MB
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: tamaños de archivo en MB
        List<double> tamanosArchivo = new List<double> { 2.5, 4.5, 3.0, 3.5, 2.0 };

        // Utilizar LINQ para calcular el tamaño promedio de archivo
        double tamanoPromedio = tamanosArchivo.Average();

        // Mostrar el resultado
        Console.WriteLine($"El tamaño promedio de archivo es de {tamanoPromedio} MB");
    }
}
```

**17.	Escriba un programa en C# para eliminar elementos de la lista usando la función eliminar pasando el objeto.**

```sh:
Datos de prueba:
Aquí está la lista de elementos:
Char: m Char: n Char: o Char: p Char: q
Salida esperada:
Aquí está la lista después de eliminar el elemento 'o' de la lista: Char: m
Char: n Char: p Char: q
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de elementos
        List<char> lista = new List<char> { 'm', 'n', 'o', 'p', 'q' };

        // Mostrar la lista original
        Console.WriteLine("Aquí está la lista de elementos:");
        foreach (var elemento in lista)
        {
            Console.WriteLine($"Char: {elemento}");
        }

        // Eliminar el elemento 'o' de la lista utilizando LINQ
        lista = lista.Where(elemento => elemento != 'o').ToList();

        // Mostrar la lista después de eliminar el elemento 'o'
        Console.WriteLine("Aquí está la lista después de eliminar el elemento 'o' de la lista:");
        foreach (var elemento in lista)
        {
            Console.WriteLine($"Char: {elemento}");
        }
    }
}
```

**18.	Escriba un programa en C# para eliminar elementos de la lista creando un objeto internamente mediante el filtrado.**

```sh:
Datos de prueba:
Aquí está la lista de elementos:
Char: m Char: n Char: o Char: p Char: q
Salida esperada :
Aquí está la lista después de eliminar el elemento 'p' de la lista: Char: m
 
Char: n Char: o Char: q
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq; // Directiva using para LINQ

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de números
        List<int> numeros = new List<int> { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };

        // Filtrar números pares utilizando LINQ
        var numerosPares = numeros.Where(numero => numero % 2 == 0); // LINQ: Where

        // Proyectar los nombres a mayúsculas utilizando LINQ
        var nombres = new List<string> { "Juan", "María", "Carlos", "Ana" };
        var nombresMayusculas = nombres.Select(nombre => nombre.ToUpper()); // LINQ: Select

        // Ordenar los números en orden descendente utilizando LINQ
        var numerosOrdenados = numeros.OrderByDescending(numero => numero); // LINQ: OrderByDescending

        // Obtener el primer número de la lista utilizando LINQ
        var primerNumero = numeros.FirstOrDefault(); // LINQ: FirstOrDefault

        // Contar la cantidad de números en la lista utilizando LINQ
        var cantidadNumeros = numeros.Count(); // LINQ: Count

        // Agrupar personas por edad utilizando LINQ
        var personas = new List<Persona>
        {
            new Persona { Nombre = "Juan", Edad = 25 },
            new Persona { Nombre = "María", Edad = 30 },
            new Persona { Nombre = "Carlos", Edad = 25 },
            new Persona { Nombre = "Ana", Edad = 30 }
        };
        var gruposPorEdad = personas.GroupBy(persona => persona.Edad); // LINQ: GroupBy

        // Mostrar resultados
        Console.WriteLine("Números pares:");
        foreach (var numero in numerosPares)
        {
            Console.WriteLine(numero);
        }

        Console.WriteLine("\nNombres en mayúsculas:");
        foreach (var nombre in nombresMayusculas)
        {
            Console.WriteLine(nombre);
        }

        Console.WriteLine("\nNúmeros ordenados en orden descendente:");
        foreach (var numero in numerosOrdenados)
        {
            Console.WriteLine(numero);
        }

        Console.WriteLine($"\nPrimer número de la lista: {primerNumero}");
        Console.WriteLine($"Cantidad de números en la lista: {cantidadNumeros}");

        Console.WriteLine("\nGrupos de personas por edad:");
        foreach (var grupo in gruposPorEdad)
        {
            Console.WriteLine($"Edad: {grupo.Key}");
            foreach (var persona in grupo)
            {
                Console.WriteLine($"Nombre: {persona.Nombre}");
            }
            Console.WriteLine();
        }
    }
}

class Persona
{
    public string Nombre { get; set; }
    public int Edad { get; set; }
}
```

**19.	Escriba un programa en C# para eliminar elementos de la lista pasando filtros.**

```sh:
Datos de prueba:
Aquí está la lista de elementos:
Char: m Char: n Char: o Char: p Char: q
Salida esperada :
Aquí está la lista después de eliminar el elemento 'q' de la lista: Char: m
Char : n Char: o Char: p
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de elementos
        List<char> elementos = new List<char> { 'm', 'n', 'o', 'p', 'q' };

        // Mostrar la lista original
        Console.WriteLine("Aquí está la lista de elementos:");
        foreach (var elemento in elementos)
        {
            Console.Write($"Char: {elemento} ");
        }
        Console.WriteLine();

        // Eliminar el elemento 'q' de la lista utilizando LINQ
        elementos = elementos.Where(e => e != 'q').ToList(); // Utilizando LINQ para filtrar y crear una nueva lista

        // Mostrar la lista después de la eliminación
        Console.WriteLine("Aquí está la lista después de eliminar el elemento 'q' de la lista:");
        foreach (var elemento in elementos)
        {
            Console.Write($"Char: {elemento} ");
        }
        Console.WriteLine();
    }
}
```

**20.	Escriba un programa en C# para eliminar elementos de la lista pasando el índice de elementos.**

```sh:
Datos de prueba:
Aquí está la lista de elementos:
Char: m Char: n Char: o Char: p Char: q
Salida esperada :
Aquí está la lista después de eliminar el índice de elementos 3 de la lista: Char: m
Char: n Char: o
 
Char: q
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de elementos
        List<char> elementos = new List<char> { 'm', 'n', 'o', 'p', 'q' };

        // Mostrar la lista original
        Console.WriteLine("Aquí está la lista de elementos:");
        foreach (var elemento in elementos)
        {
            Console.Write($"Char: {elemento} ");
        }
        Console.WriteLine();

        // Eliminar el elemento en el índice 3 de la lista utilizando LINQ
        int indiceAEliminar = 3;
        elementos = elementos.Where((e, index) => index != indiceAEliminar).ToList(); // Utilizando LINQ para filtrar y crear una nueva lista

        // Mostrar la lista después de la eliminación
        Console.WriteLine($"Aquí está la lista después de eliminar el índice de elementos {indiceAEliminar} de la lista:");
        foreach (var elemento in elementos)
        {
            Console.Write($"Char: {elemento} ");
        }
        Console.WriteLine();
    }
}
```

**21.	Escriba un programa en C# para eliminar un rango de elementos de una lista al pasar el índice de inicio y el número de elementos a eliminar.**

```sh:
Datos de prueba:
Aquí está la lista de elementos:
Char: m Char: n Char: o Char: p Char: q
Salida esperada:
Aquí está la lista después de eliminar los tres elementos que comienzan del índice de elementos 1 de la lista :
Char: m Char: q
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Datos de prueba: lista de elementos
        List<char> elementos = new List<char> { 'm', 'n', 'o', 'p', 'q' };

        // Mostrar la lista original
        Console.WriteLine("Aquí está la lista de elementos:");
        foreach (var elemento in elementos)
        {
            Console.Write($"Char: {elemento} ");
        }
        Console.WriteLine();

        // Índice de inicio y número de elementos a eliminar
        int indiceInicio = 1;
        int numeroElementosEliminar = 3;

        // Utilizando LINQ para filtrar y mantener los elementos que no están en el rango especificado
        elementos = elementos.Where((e, index) => index < indiceInicio || index >= indiceInicio + numeroElementosEliminar).ToList();

        // Mostrar la lista después de la eliminación
        Console.WriteLine($"Aquí está la lista después de eliminar los {numeroElementosEliminar} elementos que comienzan del índice de elementos {indiceInicio} de la lista:");
        foreach (var elemento in elementos)
        {
            Console.Write($"Char: {elemento} ");
        }
        Console.WriteLine();
    }
}
```

**22.	Escriba un programa en C# para encontrar las cadenas para una longitud mínima específica.**

```sh:
Datos de prueba:
Ingrese el número de cadenas que se almacenarán en la matriz: 4 Entrada 4 cadenas para la matriz:
Elemento [0]: este
Elemento [1]: es
Elemento [2]: un
Elemento [3]: cadena
Entrada la longitud mínima del elemento que desea encontrar: 5
Resultado esperado :
Los elementos de un mínimo de 5 caracteres son:
Elemento: cadena
```

Salida:

```c#:
using System;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Solicitar al usuario ingresar el número de cadenas
        Console.WriteLine("Ingrese el número de cadenas que se almacenarán en la matriz:");
        int numCadenas = int.Parse(Console.ReadLine());

        // Crear una matriz para almacenar las cadenas ingresadas
        string[] cadenas = new string[numCadenas];

        // Solicitar al usuario que ingrese las cadenas
        for (int i = 0; i < numCadenas; i++)
        {
            Console.Write($"Elemento [{i}]: ");
            cadenas[i] = Console.ReadLine();
        }

        // Solicitar al usuario que ingrese la longitud mínima deseada
        Console.WriteLine("Ingrese la longitud mínima del elemento que desea encontrar:");
        int longitudMinima = int.Parse(Console.ReadLine());

        // Utilizar LINQ para encontrar las cadenas con longitud mínima especificada
        var cadenasFiltradas = cadenas.Where(c => c.Length >= longitudMinima);

        // Mostrar los elementos con longitud mínima especificada
        Console.WriteLine($"Los elementos de un mínimo de {longitudMinima} caracteres son:");
        foreach (var cadena in cadenasFiltradas)
        {
            Console.WriteLine($"Elemento: {cadena}");
        }
    }
}
```

**23.	Escriba un programa en C# para generar un Producto Cartesiano de dos conjuntos.**

```sh
Resultado esperado :
 
Los productos cartesianos son:
{letterList = X, numberList = 1}
{letterList = X, numberList = 2}
{letterList = X, numberList = 3}
{letterList = X, numberList = 4}
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Definir los conjuntos
        List<char> letterList = new List<char> { 'X' };
        List<int> numberList = new List<int> { 1, 2, 3, 4 };

        // Generar el Producto Cartesiano utilizando LINQ
        var cartesianProduct = from letter in letterList
                               from number in numberList
                               select new { letterList = letter, numberList = number };

        // Mostrar el Producto Cartesiano
        Console.WriteLine("Los productos cartesianos son:");
        foreach (var item in cartesianProduct)
        {
            Console.WriteLine("{letterList = " + item.letterList + ", numberList = " + item.numberList + "}");
        }
    }
}
```


**24.	Escriba un programa en C# para generar un Producto Cartesiano de tres conjuntos.**

```sh:
Salida esperada :
El producto cartesiano es:
{letra = X, número = 1, color = verde}
{letra = X, número = 1, color = naranja}
{letra = X, número = 2, color = verde }
{letra = X, número = 2, color = Naranja}
{letra = X, número = 3, color = Verde}
{letra = X, número = 3, color = Naranja}
{letra = Y, número = 1, color = Verde}
{letra = Y, número = 1, color = Naranja}
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main(string[] args)
    {
        // Definir los conjuntos
        List<char> letterList = new List<char> { 'X', 'Y' };
        List<int> numberList = new List<int> { 1, 2, 3 };
        List<string> colorList = new List<string> { "verde", "naranja" };

        // Generar el Producto Cartesiano utilizando LINQ
        var cartesianProduct = from letter in letterList
                               from number in numberList
                               from color in colorList
                               select new { letra = letter, número = number, color = color };

        // Mostrar el Producto Cartesiano
        Console.WriteLine("El producto cartesiano es:");
        foreach (var item in cartesianProduct)
        {
            Console.WriteLine("{letra = " + item.letra + ", número = " + item.número + ", color = " + item.color + "}");
        }
    }
}
```

**25.	Escriba un programa en C# para generar una unión interna entre dos conjuntos de datos.**

```sh:
Resultado esperado:
ID del artículo Nombre del artículo Cantidad de compra
1 Bizcocho 458
2 Chocolate 650
3 Mantequilla 800
3 Mantequilla 900
3 Mantequilla 900
4 Brade 700
4 Brade 650
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq; // LINQ se utiliza aquí

class Program
{
    static void Main(string[] args)
    {
        // Conjunto 1: ID del artículo, Nombre del artículo, Cantidad de compra
        Console.WriteLine("Ingrese los datos para el primer conjunto:");
        List<(int, string, int)> conjunto1 = ObtenerConjunto();

        // Conjunto 2: ID del artículo, Nombre del artículo, Cantidad de compra
        Console.WriteLine("\nIngrese los datos para el segundo conjunto:");
        List<(int, string, int)> conjunto2 = ObtenerConjunto();

        // Realizar la unión interna entre los conjuntos utilizando LINQ
        var union = from item1 in conjunto1
                    join item2 in conjunto2
                    on item1.Item1 equals item2.Item1
                    select new
                    {
                        IDArticulo = item1.Item1,
                        NombreArticulo = item1.Item2,
                        CantidadCompra = item1.Item3
                    };

        // Mostrar el resultado
        Console.WriteLine("\nResultado de la unión interna:");
        Console.WriteLine("ID del artículo\tNombre del artículo\tCantidad de compra");
        foreach (var item in union)
        {
            Console.WriteLine($"{item.IDArticulo}\t\t{item.NombreArticulo}\t\t{item.CantidadCompra}");
        }
    }

    // Método para obtener los datos del conjunto
    static List<(int, string, int)> ObtenerConjunto()
    {
        List<(int, string, int)> conjunto = new List<(int, string, int)>();

        Console.Write("Ingrese el número de elementos en el conjunto: ");
        int n = int.Parse(Console.ReadLine());

        for (int i = 0; i < n; i++)
        {
            Console.WriteLine($"Elemento {i + 1}:");
            Console.Write("ID del artículo: ");
            int id = int.Parse(Console.ReadLine());
            Console.Write("Nombre del artículo: ");
            string nombre = Console.ReadLine();
            Console.Write("Cantidad de compra: ");
            int cantidad = int.Parse(Console.ReadLine());

            conjunto.Add((id, nombre, cantidad));
        }

        return conjunto;
    }
}
```

**26.	Escriba un programa en C# para generar una unión izquierda entre dos conjuntos de datos.**

```sh: 
Resultado esperado :
Aquí está la lista después de unirse:

ID del artículo Nombre del artículo Cantidad de compra
1 Bizcocho 458
2 Chocolate 650
3 Mantequilla 800
3 Mantequilla 900
3 Mantequilla 900
4 Brade 700
4 Brade 650
5 miel 0
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main()
    {
        Console.WriteLine("Ingrese los datos para la unión izquierda:");
        Console.Write("Ingrese el número de elementos a agregar: ");
        int numElementos = Convert.ToInt32(Console.ReadLine());

        List<(int, string, int)> datos = new List<(int, string, int)>();

        for (int i = 1; i <= numElementos; i++)
        {
            Console.WriteLine($"Elemento {i}:");
            Console.Write("ID del artículo: ");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nombre del artículo: ");
            string nombre = Console.ReadLine();
            Console.Write("Cantidad de compra: ");
            int cantidad = Convert.ToInt32(Console.ReadLine());
            datos.Add((id, nombre, cantidad));
        }

        Console.WriteLine("\nIngrese los datos de los artículos:");
        Console.Write("Ingrese el número de artículos a agregar: ");
        int numArticulos = Convert.ToInt32(Console.ReadLine());

        // Arreglo para almacenar los artículos
        (int, string, int)[] articulos = new (int, string, int)[numArticulos];

        for (int i = 0; i < numArticulos; i++)
        {
            Console.WriteLine($"Artículo {i + 1}:");
            Console.Write("ID del artículo: ");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nombre del artículo: ");
            string nombre = Console.ReadLine();
            Console.Write("Cantidad de compra: ");
            int cantidad = Convert.ToInt32(Console.ReadLine());
            articulos[i] = (id, nombre, cantidad);
        }

        Console.WriteLine("\nAquí está la lista después de unirse:\n");
        Console.WriteLine("ID del artículo  | Nombre del artículo | Cantidad de compra");
        Console.WriteLine("-----------------------------------------------");

        //Esta consulta LINQ realiza una unión izquierda entre dos secuencias de datos: datos y articulos. 
        var resultado = from dato in datos
                        join articulo in articulos on dato.Item1 equals articulo.Item1 into joined
                        from j in joined.DefaultIfEmpty((0, "", 0))
                        select (IDArticulo: j.Item1, NombreArticulo: j.Item2, CantidadCompra: dato.Item3);

        foreach (var item in resultado)
        {
            Console.WriteLine($"{item.IDArticulo,-17} | {item.NombreArticulo,-21} | {item.CantidadCompra,-17}");
        }
    }
}
```

**27.	Escriba un programa en C# para generar una unión externa derecha entre dos conjuntos de datos.**

```sh:
Resultado esperado :
Aquí está la lista después de unirse:

ID del artículo Nombre del artículo Cantidad de compra
3 Mantequilla 800
5 Miel 650
3 Mantequilla 900
4 Brade 700
3 Manntequilla 900
4 Brade 650
1 Bizcocho 458
```

Solucion: 

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main()
    {
        // Entrada de datos para el conjunto de artículos
        Console.WriteLine("Ingrese los datos para el conjunto de artículos:");
        Console.Write("Ingrese el número de artículos: ");
        int numArticulos = Convert.ToInt32(Console.ReadLine());

        List<(int, string, int)> articulos = new List<(int, string, int)>();

        for (int i = 1; i <= numArticulos; i++)
        {
            Console.WriteLine($"Artículo {i}:");
            Console.Write("ID del artículo: ");
            int id = Convert.ToInt32(Console.ReadLine());
            Console.Write("Nombre del artículo: ");
            string nombre = Console.ReadLine();
            Console.Write("Cantidad de compra: ");
            int cantidad = Convert.ToInt32(Console.ReadLine());
            articulos.Add((id, nombre, cantidad));
        }

        // Se realiza la operación de unión externa derecha entre la lista de articulos ingresados por el usuario y la lista de datos previamente almacenados, utilizando LINQ.
        var resultado = from articulo in articulos
                        join dato in datos on articulo.Item1 equals dato.Item1 into joined
                        from j in joined.DefaultIfEmpty((0, "", 0))
                        select (IDArticulo: j.Item1, NombreArticulo: j.Item2, CantidadCompra: j.Item3);

        // Salida de resultados
        Console.WriteLine("\nAquí está la lista después de unirse:\n");
        Console.WriteLine("ID del artículo  | Nombre del artículo | Cantidad de compra");
        Console.WriteLine("-----------------------------------------------");

        foreach (var item in resultado)
        {
            Console.WriteLine($"{item.IDArticulo,-17} | {item.NombreArticulo,-21} | {item.CantidadCompra,-17}");
        }
    }
}
```

**28.	Escriba un programa en C# para mostrar la lista de elementos en la matriz según la longitud de la cadena y luego por nombre en orden ascendente.**

```sh:
Resultado esperado:
Aquí está la lista ordenada: ROMA
PARIS LONDRES ZURICH NAIROBI ABU DHABI AMSTERDAM
NUEVA DELHI
 
CALIFORNIA
```

Solucion:

```c#:
using System;
using System.Linq;

class Program
{
    static void Main()
    {
        // Datos de muestra: lista de ciudades
        string[] ciudades = { "ROMA", "PARIS", "LONDRES", "ZURICH", "NAIROBI", "ABU DHABI", "AMSTERDAM", "NUEVA DELHI" };

        // Consulta LINQ para ordenar la lista de ciudades por longitud de cadena y luego por nombre
        var resultado = ciudades.OrderBy(c => c.Length).ThenBy(c => c);

        // Imprimir el resultado ordenado
        Console.WriteLine("Aquí está la lista ordenada:");
        foreach (var ciudad in resultado)
        {
            Console.WriteLine(ciudad);
        }
    }
}
```

**29.	Escriba un programa en C# para dividir una colección de cadenas en algunos grupos.**

```sh:
Resultado esperado:
Aquí está el grupo de ciudades:

ROMA; LONDRES; NAIROBI
-	aquí hay un grupo de ciudades -

CALIFORNIA; ZURICH; NUEVA DELHI
-	aquí hay un grupo de ciudades -

AMSTERDAM; ABU DHABI; PARIS
-	aquí hay un grupo de ciudades -

NUEVA YORK
-	aquí hay un grupo de ciudades -
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main()
    {
        // Datos de muestra: lista de ciudades
        string[] ciudades = { "ROMA", "LONDRES", "NAIROBI", "CALIFORNIA", "ZURICH", "NUEVA DELHI", "AMSTERDAM", "ABU DHABI", "PARIS", "NUEVA YORK" };

        // Tamaño del grupo
        int tamanoGrupo = 3;

        // Dividir la lista de ciudades en grupos usando LINQ.
        var grupos = ciudades.Select((ciudad, indice) => new { Ciudad = ciudad, IndiceGrupo = indice / tamanoGrupo })
                             .GroupBy(x => x.IndiceGrupo, y => y.Ciudad)
                             .Select(grupo => string.Join("; ", grupo) + "\n- aquí hay un grupo de ciudades -");

        // Imprimir los grupos
        Console.WriteLine("Aquí está el grupo de ciudades:\n");
        foreach (var grupo in grupos)
        {
            Console.WriteLine(grupo + "\n");
        }
    }
}
```

**30.	Escriba un programa en C# para organizar los distintos elementos de la lista en orden ascendente.**

```sh
Resultado previsto: Biscuit
Brade Butter Honey
```

Solucion:

```c#:
using System;
using System.Collections.Generic;
using System.Linq;

class Program
{
    static void Main()
    {
        // Datos de muestra: lista de elementos
        List<string> elementos = new List<string> { "Butter", "Biscuit", "Honey", "Brade" };

        // Organizar los elementos en orden ascendente usando LINQ.
        var elementosOrdenados = elementos.OrderBy(elemento => elemento);

        // Imprimir los elementos ordenados
        Console.WriteLine("Resultado previsto:");
        foreach (var elemento in elementosOrdenados)
        {
            Console.WriteLine(elemento);
        }
    }
}
```

# Mostrado los ejercicios resultos, resolver los ejercicios propuestos

* **Nota:** 
* **Codificar los ejercicios en c#**
* **Emplear LINQ para solucionar los ejercicios.**
* **Documentar a mano cada solución implementada.**
* **Los codigos deben ser guardados en vuestros repositorios**
* **Para el trabajo debe ser entregado mediante un ejecutable que se pueda ejecutar desde consola**

1. Concatenar dos listas eliminando duplicados. (2 pts)
2. Calcular la suma de los cuadrados de los números en una lista. (2 pts)
3. Filtrar una lista de objetos por un rango de valores de una propiedad numérica. (2 pts)
4. Filtrar una lista de objetos por un rango de valores de una propiedad numérica. (2 pts)
5. Encontrar la moda (valor que aparece con mayor frecuencia) en una lista de números. (3pts)
6. Eliminar elementos duplicados de una lista mientras se conserva el orden original. (3pts)
7. Realizar una operación de unión completa entre dos listas de objetos. (3pts)
8. Realizar una operación de partición en una lista de objetos basada en una condición. (3pts)
